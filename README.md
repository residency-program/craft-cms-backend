# Craft-CMS

In this repository we are creating a service to manage our craft-cms application.


To start the project -

1. Install all the dependencies in the system which are required to run craft-cms thorough the server.

2. Install DDEV on your local machine. It is a Docker-based PHP development environment that streamlines the creation and management of resources required by a Craft project.

3. Add the following properties in the .env file
# The application ID used to to uniquely store session and cache data, mutex locks, and more
CRAFT_APP_ID=CraftCMS--5be41343-3378-431d-9508-f6dabc4b9de0

# The environment Craft is currently running in (dev, staging, production, etc.)
CRAFT_ENVIRONMENT=dev

# The secure key Craft will use for hashing and encrypting data
CRAFT_SECURITY_KEY=GBkbdx6a_vJuO_8ev7dNS6Ct7GqtWj0l

# Database connection settings
CRAFT_DB_DRIVER="mysql"
CRAFT_DB_SERVER="db"
CRAFT_DB_PORT="3306"
CRAFT_DB_DATABASE="db"
CRAFT_DB_USER="db"
CRAFT_DB_PASSWORD="db"
CRAFT_DB_SCHEMA=public
CRAFT_DB_TABLE_PREFIX=

# General settings (see config/general.php)
DEV_MODE=true
ALLOW_ADMIN_CHANGES=true
DISALLOW_ROBOTS=true
PRIMARY_SITE_URL="https://my-craft-project.ddev.site"
MAILHOG_SMTP_HOSTNAME="127.0.0.1"
MAILHOG_SMTP_PORT="1025"

ALLOW_GRAPHQL_ORIGINS=true
ENABLE_GQL=true
ENABLE_GRAPHQL_CACHING=true
GQL_TYPE_PREFIX=craft_


4. To run craft-cms on local machine through terminal:-

`ddev start`
`ddev launch`

5. To stop local server:-

`ddev stop`